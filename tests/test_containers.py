import unittest

import numpy
import pandas

from d3m_metadata import container


class TestDataset(unittest.TestCase):
    def test_list(self):
        l = container.List[int]()

        self.assertTrue(hasattr(l, 'metadata'))

        class IntList(container.List[int]):
            pass

        l = IntList([1, 2, 3])

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertIsInstance(l, IntList)
        self.assertTrue(hasattr(l, 'metadata'))
        self.assertIs(l.metadata.for_value, l)

        self.assertIsInstance(l, container.List)
        self.assertIsInstance(l, list)

        self.assertNotIsInstance([], container.List)
        self.assertNotIsInstance(container.List(), IntList)

        l_copy = l[:]

        self.assertIsInstance(l_copy, IntList)
        self.assertTrue(hasattr(l_copy, 'metadata'))
        self.assertIs(l_copy.metadata.for_value, l_copy)
        self.assertSequenceEqual(l, l_copy)

        self.assertEqual(l[1], 2)

        with self.assertRaisesRegex(TypeError, 'list indices must be integers or slices, not tuple'):
            l[1, 2]

        l_slice = l[1:3]

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertSequenceEqual(l_slice, [2, 3])
        self.assertIsInstance(l_slice, IntList)
        self.assertTrue(hasattr(l_slice, 'metadata'))
        self.assertIs(l_slice.metadata.for_value, l_slice)

        l_added = l + [4, 5]

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertSequenceEqual(l_added, [1, 2, 3, 4, 5])
        self.assertIsInstance(l_added, IntList)
        self.assertTrue(hasattr(l_added, 'metadata'))
        self.assertIs(l_added.metadata.for_value, l_added)

        l_added += [6, 7]

        self.assertSequenceEqual(l_added, [1, 2, 3, 4, 5, 6, 7])
        self.assertIsInstance(l_added, IntList)
        self.assertTrue(hasattr(l_added, 'metadata'))
        self.assertIs(l_added.metadata.for_value, l_added)

        l_multiplied = l * 3

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertSequenceEqual(l_multiplied, [1, 2, 3, 1, 2, 3, 1, 2, 3])
        self.assertIsInstance(l_multiplied, IntList)
        self.assertTrue(hasattr(l_multiplied, 'metadata'))
        self.assertIs(l_multiplied.metadata.for_value, l_multiplied)

        l_multiplied = 3 * l

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertSequenceEqual(l_multiplied, [1, 2, 3, 1, 2, 3, 1, 2, 3])
        self.assertIsInstance(l_multiplied, IntList)
        self.assertTrue(hasattr(l_multiplied, 'metadata'))
        self.assertIs(l_multiplied.metadata.for_value, l_multiplied)

        l_multiplied *= 2

        self.assertSequenceEqual(l_multiplied, [1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3])
        self.assertIsInstance(l_multiplied, IntList)
        self.assertTrue(hasattr(l_multiplied, 'metadata'))
        self.assertIs(l_multiplied.metadata.for_value, l_multiplied)

    def test_ndarray(self):
        array = container.ndarray(numpy.array([1, 2, 3]))
        self.assertTrue(numpy.array_equal(array, [1, 2, 3]))
        self.assertIsInstance(array, container.ndarray)
        self.assertTrue(hasattr(array, 'metadata'))
        self.assertIs(array.metadata.for_value, array)

        self.assertIsInstance(array, numpy.ndarray)

        self.assertNotIsInstance(numpy.array([]), container.ndarray)

        array_copy = array[:]

        self.assertIsInstance(array_copy, container.ndarray)
        self.assertTrue(hasattr(array_copy, 'metadata'))
        self.assertIs(array_copy.metadata.for_value, array_copy)

        self.assertTrue(numpy.array_equal(array, array_copy))

        array_from_list = container.ndarray([1, 2, 3])
        self.assertTrue(numpy.array_equal(array_from_list, [1, 2, 3]))
        self.assertIsInstance(array_from_list, container.ndarray)
        self.assertTrue(hasattr(array_from_list, 'metadata'))
        self.assertIs(array_from_list.metadata.for_value, array_from_list)

    def test_matrix(self):
        matrix = container.matrix(numpy.array([[1, 2], [3, 4]]))
        self.assertTrue(numpy.array_equal(matrix, [[1, 2], [3, 4]]))
        self.assertIsInstance(matrix, container.matrix)
        self.assertTrue(hasattr(matrix, 'metadata'))
        self.assertIs(matrix.metadata.for_value, matrix)

        self.assertIsInstance(matrix, numpy.ndarray)
        self.assertIsInstance(matrix, numpy.matrix)
        self.assertIsInstance(matrix, container.ndarray)

        self.assertNotIsInstance(numpy.matrix([]), container.matrix)

        matrix_copy = matrix[:]

        self.assertIsInstance(matrix_copy, container.matrix)
        self.assertTrue(hasattr(matrix_copy, 'metadata'))
        self.assertIs(matrix_copy.metadata.for_value, matrix_copy)

        self.assertTrue(numpy.array_equal(matrix, matrix_copy))

        matrix_from_list = container.matrix([[1, 2], [3, 4]])
        self.assertTrue(numpy.array_equal(matrix_from_list, [[1, 2], [3, 4]]))
        self.assertIsInstance(matrix_from_list, container.matrix)
        self.assertTrue(hasattr(matrix_from_list, 'metadata'))
        self.assertIs(matrix_from_list.metadata.for_value, matrix_from_list)

    def test_dataframe(self):
        df = container.DataFrame(pandas.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}))
        self.assertTrue(df._data.equals(pandas.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]})._data))
        self.assertIsInstance(df, container.DataFrame)
        self.assertTrue(hasattr(df, 'metadata'))
        self.assertIs(df.metadata.for_value, df)

        self.assertIsInstance(df, pandas.DataFrame)

        self.assertNotIsInstance(pandas.DataFrame({'A': [1, 2, 3]}), container.DataFrame)

        df_copy = df[:]

        self.assertIsInstance(df_copy, container.DataFrame)
        self.assertTrue(hasattr(df_copy, 'metadata'))
        self.assertIs(df_copy.metadata.for_value, df_copy)

        self.assertTrue(df.equals(df_copy))

        df_from_dict = container.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]})
        self.assertTrue(df_from_dict._data.equals(pandas.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]})._data))
        self.assertIsInstance(df_from_dict, container.DataFrame)
        self.assertTrue(hasattr(df_from_dict, 'metadata'))
        self.assertIs(df_from_dict.metadata.for_value, df_from_dict)

    def test_sparse_dataframe(self):
        sparse = container.SparseDataFrame(pandas.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}))
        self.assertTrue(sparse._data.equals(pandas.SparseDataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]})._data))
        self.assertIsInstance(sparse, container.SparseDataFrame)
        self.assertTrue(hasattr(sparse, 'metadata'))
        self.assertIs(sparse.metadata.for_value, sparse)

        self.assertIsInstance(sparse, pandas.DataFrame)
        self.assertIsInstance(sparse, pandas.SparseDataFrame)
        self.assertIsInstance(sparse, container.DataFrame)

        self.assertNotIsInstance(pandas.SparseDataFrame({'A': [1, 2, 3]}), container.SparseDataFrame)

        sparse_copy = sparse[:]

        self.assertIsInstance(sparse_copy, container.SparseDataFrame)
        self.assertTrue(hasattr(sparse_copy, 'metadata'))
        self.assertIs(sparse_copy.metadata.for_value, sparse_copy)

        self.assertTrue(sparse.equals(sparse_copy))

        sparse_from_dict = container.SparseDataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]})
        self.assertTrue(sparse_from_dict._data.equals(pandas.SparseDataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]})._data))
        self.assertIsInstance(sparse_from_dict, container.SparseDataFrame)
        self.assertTrue(hasattr(sparse_from_dict, 'metadata'))
        self.assertIs(sparse_from_dict.metadata.for_value, sparse_from_dict)


if __name__ == '__main__':
    unittest.main()
